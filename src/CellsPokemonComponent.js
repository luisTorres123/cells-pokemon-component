import { LitElement, html, } from 'lit-element';
import { getComponentSharedStyles } from '@bbva-web-components/bbva-core-lit-helpers';
import styles from './CellsPokemonComponent-styles.js';

import '@bbva-web-components/bbva-web-button-default/bbva-web-button-default.js';
import '@bbva-web-components/bbva-web-divider/bbva-web-divider.js';
import '@bbva-web-components/bbva-web-list-item-simple/bbva-web-list-item-simple.js';
import '@bbva-web-components/bbva-web-template-modal/bbva-web-template-modal.js';
import "@capa-cells/cells-pokeapi-dm/cells-pokeapi-dm.js";
/**
![LitElement component](https://img.shields.io/badge/litElement-component-blue.svg)

This component ...

Example:

```html
<cells-pokemon-component></cells-pokemon-component>
```

##styling-doc

@customElement cells-pokemon-component
*/
export class CellsPokemonComponent extends LitElement {
  static get is() {
    return 'cells-pokemon-component';
  }

  // Declare properties
  static get properties() {
    return {
      pageKey: { type: Number },
      pageSize: { type: Number },
      previous: { type: String },
      next: { type: String },
      pokemonList: { type : Object },
      detail: { type: Object }
    };
  }

  // Initialize properties
  constructor() {
    super();
    this.pageKey = 0;
    this.pageSize = 20;
    this.previous = '<';
    this.next = '>';
    setTimeout(() => {
      this.shadowRoot.getElementById('pokemonDm').getPokemonList(this.pageKey, this.pageSize);
    }, 1000);
  }

  static get styles() {
    return [
      styles,
      getComponentSharedStyles('cells-pokemon-component-shared-styles')
    ];
  }

  render() {
    return html`
      <slot></slot>
      <div class="buttons">
        <bbva-web-button-default size="l" ?disabled="${this.pageKey === 0}" @click="${() => this._getList('P')}">${this.previous}</bbva-web-button-default>
        <span>${this.pageKey + 1}</span>
        <bbva-web-button-default size="l" @click="${() => this._getList('N')}">${this.next}</bbva-web-button-default>
      </div>
      <div class="title">
        <span>POKEMON LIST</span>
      </div>
      <bbva-web-divider></bbva-web-divider>
      ${this.pokemonList ? this._showList: ''}
      <bbva-web-template-modal size="full" heading="${this.detail ? this.detail.name : ''}">
        <img src="${this.detail ? this.detail.sprites.other['official-artwork'].front_default : ''}" alt="pokemon">
        <div class="moves">
          <div class="title">
            <span>MOVES</span>
          </div>
          <ul>
            ${this.detail ? this._getMoves() : ''}
          </ul>
        </div>
      </bbva-web-template-modal>
      <cells-pokeapi-dm id="pokemonDm"
        @list-success="${this._pokemonList}"
        @detail-success="${this._pokemonDetail}">
      </cells-pokeapi-dm>
    `;
  }

  get _showList() {
    return this.pokemonList.map(pokemon => this._showPokemon(pokemon));
  }

  _showPokemon(pokemon) {
    return html`
      <bbva-web-list-item-simple label="${pokemon.name}" @click=${() => { this._getDetail(pokemon.url); }}">
      </bbva-web-list-item-simple>
      <bbva-web-divider></bbva-web-divider>
    `;
  }

  _getList(option) {
    if (option === 'P') {
      this.pageKey--;
    } else {
      this.pageKey++;
    }
    this.shadowRoot.getElementById('pokemonDm').getPokemonList(this.pageKey, this.pageSize);
  }

  _getDetail(url) {
    this.shadowRoot.getElementById('pokemonDm').getPokemonDetail(url);
  }

  _pokemonDetail(ev) {
    this.detail = ev.detail;
    setTimeout(() => {
      this.shadowRoot.querySelector('bbva-web-template-modal').open();  
    }, 1000);
  }

  _pokemonList(ev) {
    this.pokemonList = ev.detail.results;
  }

  _getMoves() {
    return this.detail.moves.map(move => this._showMove(move));
  }

  _showMove(move) {
    return html`
      <li>
        ${move.move.name}
      </li>
    `;
  }
}
